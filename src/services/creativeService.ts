export const getCreativesCount = async () =>  {

    const response = await fetch(
      `http://localhost:3001/creatives`);
  
    const responseData = await response.json();
    
  
    if (!response.ok) {
      //error
      const error = new Error(responseData.message || 'Failed to fetch!');
      throw error;
    }
  
    return responseData.length;
  }