import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@mui/styles';
import Typography from '@mui/material/Typography';
import useMediaQuery from '@mui/material/useMediaQuery';
import { Grid, Paper, Switch, Chip, Container } from '@mui/material';
import { Avatar } from '@mui/material';
import { FormatsEntity, ICreatives, TContributorsEntity } from '../type';
//import {useNavigate} from 'react-router-dom';

const useStyles = makeStyles({
    card: {
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 10,
        marginRight: 10,
    },
    clink: {
        textDecoration: 'none',
    },
    container: {
        width: '60%',
        margin: 'auto',
    },
    icon: {
        marginRight: 15,
    },
    image: {
        marginTop: 10,
         maxWidth: '50%',
        margin: 'auto',
    }
    ,
    action: {
        justifyContent: 'space-between'
    }
});

const CreativeListItem : React.FC<ICreatives> = ({id,title, description, contributors, formats,enabled }) => {
    //const navigate = useNavigate();


    const matches = useMediaQuery('(min-width:600px)');

    const classes = useStyles();

    return (
    
      <Grid container style={{ marginTop: 16, marginBottom: 16, justifyContent: 'center'}} spacing={3}>
      <Grid item xs={10}>
        <Paper style={{ padding: 16 }} elevation={8}>

                    <Grid container spacing={1}>
                      <Grid item xs={5}>
                      <Link className={classes.clink} to={`/displaydetail/${id}`}>
                        <Typography
                          variant="h6"
                        >
                          {title}
                        </Typography>
                        </Link>
                      </Grid>
                      <Grid item xs={3} style={{marginLeft: 1, justifyContent: 'center'}}>
                        <div style={{ display: "flex" }}>
                          {contributors.map((contributor: TContributorsEntity) => (
                            <Avatar key={contributor.id} style={{ marginLeft: -10 }}>
                              {contributor.firstName.charAt(0)+contributor.lastName.charAt(0)}
                            </Avatar>
                          ))}
                        </div>
                      </Grid>
                      <Grid item xs={3}>
                        {formats.map((format: FormatsEntity, index: number) => {
                           const dims = format.height+" x "+ format.width
                           return <Chip
                            style={{ marginRight: 8 }}
                            key={index}
                            label={dims}
                          />
                        }
                        )}
                      </Grid>
                      <Grid item >
                      <Switch checked={enabled} />
                      </Grid>
                    </Grid>

         </Paper>
      </Grid>
      </Grid>
   
    )
}
export default CreativeListItem;