import React from 'react';
import {
    BrowserRouter,
    Routes,
    Route,
  } from "react-router-dom";
import CreativesList from '../components/CreativesList';
import DisplayDetail from '../components/DisplayDetail';
import ModifyDetails from '../components/ModifyDetails';

const AppRouter = ( ) => (

<BrowserRouter>
    <Routes>
      <Route path="/" element={<CreativesList/>}/>
      <Route path="/displaydetail/:id" element={<DisplayDetail/>} />
      <Route path="/modify/:id" element={<ModifyDetails/>} />
    </Routes>
  </BrowserRouter>
);

export default AppRouter;

