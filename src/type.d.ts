export interface ICreatives {
  id: string;
  createdBy: CreatedBy;
  contributors?: (ContributorsEntity)[] | null;
  lastModified: string;
  enabled: boolean;
  title: string;
  description: string;
  content: string;
  formats?: (FormatsEntity)[] | null;
}
export interface CreatedBy {
  firstName: string;
  lastName: string;
}
export interface ContributorsEntity {
  id: string;
  firstName: string;
  lastName: string;
}
export interface FormatsEntity {
  width: number;
  height: number;
}

type TCreativeFormInput = {
title: string
description: string
content: string
enabled?: boolean
};

type TContributorsEntity =  {
  id: string;
  firstName: string;
  lastName: string;
}
type TItemState = {
  inputValues: {
    title: string;
    description:string;
    content:string;
    enabled?: boolean
   },
  inputValidities: {
    title: boolean;
    description: boolean;
    content:boolean;
    enabled?: boolean
  },
  formIsValid: boolean
}

type TupdatedValidities = {
  title: boolean;
  description: boolean;
  content:boolean;
}

type Taction = {
  type: string;
  input: string;
  value: string  | boolean;
  isValid: boolean;
}