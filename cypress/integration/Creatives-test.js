describe("payment", () => {
  it("User can make payment", () => {
       //  login
        cy.visit('http://localhost:3000');       
      //  cy.findByRole('link', { name: /Test Title AAA/i }).click()
      //  cy.findByRole('button').click()
      //cy.log("No of Creatives",detail.length);
      // Get list
      // detail.toArray().forEach(det => cy.log("TD detail ==> ",det.innerHTML));

      cy.get('h6')
       .then((detail) => {
        cy.log("Check no of rows on first page");
        expect(detail.length).equal(4)
         detail[0].click()
        });
        cy.log("Check values on display screen");
        cy.findByText('OLD Title Used For tests do not change or delete').should('be.visible');
        cy.findByText('OLD Description Used For tests do not change or delete').should('be.visible');
        cy.findByText('OLD Content Used For tests do not change or delete').should('be.visible');
        // Select Edit Pen
         cy.findByRole('button').click()
       
         // Enter Details
         cy.findByRole('textbox', { name: /Titre/i }).clear().type('New Title')
          // Save 
         cy.findByRole('button', { name: /SAUVEGARDER/i }).click()
         // Make Sure List is updated
         cy.visit('http://localhost:3000'); 
         cy.findByText('New Title').should('be.visible');
         //
         // Reset to Old Value
         cy.get('h6')
         .then((detail) => {
          cy.log("Check no of rows on first page");
           detail[0].click()
          });
          cy.findByRole('button').click()
          cy.findByRole('textbox', { name: /Titre/i }).clear().type('OLD Title Used For tests do not change or delete')
          cy.findByRole('textbox', { name: /Description/i }).clear().type('OLD Description Used For tests do not change or delete')
          cy.findByRole('textbox', { name: /Contenu/i }).clear().type('OLD Content Used For tests do not change or delete')
          cy.findByRole('button', { name: /SAUVEGARDER/i }).click()
          cy.visit('http://localhost:3000');   
          cy.findByText('OLD Title Used For tests do not change or delete').should('be.visible');
    
          
          cy.log("Modify creative to test delete");
          cy.get('h6')
          .then((detail) => {
             detail[3].click()
           });
           cy.findByRole('button').click()
           cy.findByRole('textbox', { name: /Titre/i }).clear().type('Delete creative')
           cy.findByRole('button', { name: /SAUVEGARDER/i }).click()
           cy.visit('http://localhost:3000');   
           cy.findByText('Delete creative').should('be.visible');

           cy.log("Delete Item created above");
           cy.findByText('Delete creative').click();
            cy.findByRole('button').click()
            cy.findByRole('button', { name: /SUPPRIMER/i }).click()
            cy.visit('http://localhost:3000');   
            cy.log("Delete Item should no longer exist");
            cy.findByText('Delete creative').should('not.exist');
          
      
    
  });
});